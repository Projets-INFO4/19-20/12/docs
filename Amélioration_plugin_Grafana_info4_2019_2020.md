# Fiche de suivi, plugins Grafana (INFO4, 2019-2020)

## Semaine 0 : 27/01 - 02/02

Cours sur LoraWan où nous avons pu poser quelques questions sur notre objectif.

Nous devons modifier un plugin de Grafana, TrackMap afin de :

 - Importer des données/documents [GeoJSON](https://air.imag.fr/index.php/GeoJSON) et afficher des points
 - Afficher des polygones fermés (bâtiments) dans le panel
 - Ajout de popup avec les valeurs d'une requête lors du survol d'un point de cartes ou des panels adjacents
 - Créer des objets (points, lignes, formes) sur la carte un peu comme sur geojson.io sans passer par une importation

Deuxième plugin, à créer :

 - Afficher chronologiquement les images prises de camera IP (still image) comme les DLink DCS 5222L (dispo au fablab fabMSTIC).

*TODO :* 

  - Regarder comment fonctionne [Leaflet](https://leafletjs.com/)
  - Fiche à faire sur le dépôt GitLab (README.md)

## Semaine 1 : 03/02 - 09/02

Installation de [Grafana](https://grafana.com/grafana/download) ([Github](https://github.com/grafana/grafana), [Getting started](https://grafana.com/docs/grafana/latest/guides/getting_started/)),
des plugins [Track Map](https://grafana.com/grafana/plugins/alexandra-trackmap-panel) ([Github](https://github.com/alexandrainst/alexandra-trackmap-panel/blob/master/LICENSE))
et [Trackmap](https://grafana.com/grafana/plugins/pr0ps-trackmap-panel/installation) ([Github](https://github.com/pR0Ps/grafana-trackmap-panel)).

On utilisera plutôt le deuxième (sans espace), du moins dans un premier temps, car c'est ce qui était prévu.
Cependant, le premier (avec espace) paraît intéressant étant donné qu'il permet l'importation de données en GeoJSON.

Il nous sera sûrement utile pour comprendre comment importer des données GeoJSON.

Accès au [Dashboard Grafana](http://localhost:3000/) en local.

On a mis en place une page [Notion](https://www.notion.so/Am-lioration-de-greffons-Grafana-66a8e42c16bd4bc3ac3f50d1273b2adf) pour garder une trace de ce qu'on fait pendant le projet,
centraliser les ressources et organiser les tâches à faire.

*TODO :*
 
- Regarder comment fonctionne InfluxDB
    - Comment le lancer
    - Comment mettre/récupérer des données
 - Mettre à jour la page Wiki AIR avec les instructions d'installation de Grafana
 (page [Download](https://grafana.com/grafana/download) et [Configuration](https://grafana.com/docs/grafana/latest/installation/configuration/))

## Semaine 2 : 10/02 - 16/02

Installation de InfluxDB :

```bash
# Si la précédente installation d'InfluxDB n'a pas fonctionné
sudo apt remove influxdb
sudo apt update
sudo apt upgrade
sudo apt autoremove
sudo apt autoclean
sudo dpkg --configure -a
sudo rm -f /var/lib/dpkg/info/influxdb.postinst
sudo apt clean
# Si c'est la première installation d'InfluxDB
wget https://dl.influxdata.com/influxdb/releases/influxdb_1.7.9_amd64.deb
sudo dpkg -i influxdb_1.7.9_amd64.deb
# Pour tester
sudo influxd # dans un terminal qui devra rester ouvert
influx
```

Importation d'une BDD : [https://docs.influxdata.com/influxdb/v1.7/query_language/data_download/](https://docs.influxdata.com/influxdb/v1.7/query_language/data_download/)

Nous avons créé une base de donnée `Test` avec une table `Location` et pour représenter les points, des champs `latitude` et `longitude`. 

Nous avons réussi à utiliser TrackMap sur la base de donnée que nous avions créée avec InfluxDB.

TrackMap (lignes) :

```bash
SELECT median("latitude"), median("longitude") FROM "location" WHERE $timeFilter GROUP BY time($interval)
```

*TODO :*

- Demander au groupe de La Mure (Aleck et Myriam) un accès au dashboard de La Mure.
- Faire en sorte que les utilisateurs puissent ajouter des données GeoJSON pour décorer la carte sur laquelle il y a les traces des points
(borne incendie, bâtiment, poubelle...) pour voir par exemple à côté de quelle poubelle le camion est passé. 
- Juste faire la copie du GeoJSON sur la carte de Grafana. Soit en copiant collant, soit en important un fichier. 
- Fournir une page sur laquelle on peut soit copier/coller soit charger un fichier JSON.
- Utiliser Leaflet pour afficher les données GeoJSON sur la carte. 

Il faudra forker le projet du plugin existant et faire une pull request quand nos modification seront propres et bien testées.

## Semaine 3 : 16/02 - 23/02

Documentation Grafana : [https://grafana.com/docs/grafana/latest/](https://grafana.com/docs/grafana/latest/)

Grafana developer guide : [https://grafana.com/docs/grafana/latest/plugins/developing/development/](https://grafana.com/docs/grafana/latest/plugins/developing/development/)

Grafana styling : [https://github.com/grafana/grafana/tree/master/public/sass](https://github.com/grafana/grafana/tree/master/public/sass)

Nous avons cherché comment créer et utiliser un plugin personnalisé :
pour cela, il suffit de créer un dossier dans `/var/lib/grafana/plugins/` et de redémarrer le serveur.

```bash
# Installation du plugin custom
sudo chmod -R 777 /var/lib/grafana/plugins
cd /var/lib/grafana/plugins
git clone https://github.com/micamur/grafana-trackmap-panel pr0ps-trackmap-panel
cd pr0ps-trackmap-panel
npm install grunt-cli

# dans le .zshrc rajouter à la fin :
# export PATH="/usr/local/share/npm/bin:$PATH"

# Compilation à faire à chaque modification
npm run build
```

Mica a cherché comment importer un fichier geoJSON :
tutoriel panel horloge ([1](https://grafana.com/blog/2016/04/08/timing-is-everything.-writing-the-clock-panel-plugin-for-grafana-3.0/),
[2](https://grafana.com/blog/2016/04/15/timing-is-everything.-editor-mode-in-grafana-3.0-for-the-clock-panel-plugin/)), dans la patie "Editor Mode"

Les différentes parties les plus importantes du plugin :

- Code du panel : [https://github.com/pR0Ps/grafana-trackmap-panel/blob/master/src/partials/options.html](https://github.com/pR0Ps/grafana-trackmap-panel/blob/master/src/partials/options.html)
- Style du panel : [https://github.com/pR0Ps/grafana-trackmap-panel/blob/master/src/partials/module.css](https://github.com/pR0Ps/grafana-trackmap-panel/blob/master/src/partials/module.css)
- Code du plugin : [https://github.com/pR0Ps/grafana-trackmap-panel/blob/master/src/trackmap_ctrl.js](https://github.com/pR0Ps/grafana-trackmap-panel/blob/master/src/trackmap_ctrl.js)

Claire a cherché comment afficher des données GeoJSON sur la carte (recherche sur Leaflet) : il faut utiliser `L.geoJSON(geojsonFeature).addTo(map);`

Définition d'un nouvel objectif : ajouter un QR code vers un dashboard.
Dans un dashboard > Settings > Links > Add Dashboard Link > Ajouter un 3ème type qui permettrait de mettre un lien de type QR code.

### Résultats

Nous avons réussi à ajouter un bouton pour importer un fichier GeoJSON (pour l'instant seulement le bouton, il ne fait rien).

Nous avons réussi à ajouter une boîte de texte dans laquelle on peut copier/coller du code GeoJSON et lorsqu'on colle on affiche sur la carte les formes.

Nous avons forké le projet déjà existant et y avons ajouté nos modifications. 

## Semaine 4 : 02/03 - 08/03

Nous avons recherché comment importer un fichier dans les options à partir du système. Nous avons trouvé comment cela a été fait de deux manières différentes :

- [importation d'un Dashboard dans Grafana](https://github.com/grafana/grafana/blob/master/public/app/features/manage-dashboards/components/UploadDashboard/uploadDashboardDirective.ts)
- [exemple minimal en Angular](https://embed.plnkr.co/plunk/2vgnFe)

Nous avons cherché le code qui correspondait à l'importation d'un dashboard, et nous avons essayer de comprendre comment il fonctionnait.

Nous nous sommes également renseingnées sur Angular et Javascript, et nous avons essayer de comprendre comment nous pourrions faire avec un exemple
car comprendre sur le code du projet Grafana fut compliqué.

Nous avons également lu la charte de style des plugins Grafana :
[https://grafana.com/docs/grafana/latest/plugins/developing/code-styleguide/](https://grafana.com/docs/grafana/latest/plugins/developing/code-styleguide/)

*Article intéressant à propos du futur style de Grafana :
[https://grafana.com/blog/2019/10/30/new-form-styles-coming-to-grafana/](https://grafana.com/blog/2019/10/30/new-form-styles-coming-to-grafana/)*

Article à propos du mode Édition d'un plugin :
[https://grafana.com/docs/grafana/latest/plugins/developing/defaults-and-editor-mode/](https://grafana.com/docs/grafana/latest/plugins/developing/defaults-and-editor-mode/)

Première page de la section "Développement de plugins" :
[https://grafana.com/docs/grafana/latest/plugins/developing/development/](https://grafana.com/docs/grafana/latest/plugins/developing/development/)

## Semaine 5 : 09/03 - 15/03

Nous avons fait la présentation de mi-projet et nous avons également regardé la présentation d'autres groupes.

Nous avons cherché comment ajouter un fichier GeoJSON à la carte via le bouton.
Après plusieurs recherches nous avons d'abord essayé d'utiliser l'exemple de ce site [https://embed.plnkr.co/plunk/2vgnFe](https://embed.plnkr.co/plunk/2vgnFe).

Après plusieurs essais nous avons utilisé les informations sur ce site [How to open file dialog via js?](https://stackoverflow.com/questions/16215771/how-open-select-file-dialog-via-js/16215950).
Nous avons pu lancer, au clic du bouton, une fenêtre sur laquelle nous pouvons cliquer sur un fichier.
Nous avons ajouté la fonctionnalité qui permet à l'utilisateur de ne selectionner qu'un fichier `.json`. 
Nous verrons plus tard si cette fonctionnalité est pertinente ou pas.

Pour l'instant nous n'avons pas encore récupéré le contenu du fichier pour l'ajouter à la carte.
Une fois que nous aurons compris comment récupérer le contenu du fichier,
la suite ne sera pas compliquée puisqu'elle pourra certainement être gérée de la même façon que les données dans la zone de texte.

En parallèle nous avons remarqué que si le code mis dans la zone de texte est effacé, le dessin reste sur la carte.
Nous avons essayé de chercher comment mettre à jour la carte en fonction du contenu de la zone de texte.
En effet cela peut être intérressant d'ajouter plusieurs formes une à une via la zone de texte mais cela ne permet donc que d'ajouter et pas de retirer des formes.
C'est donc une fonctionnalité à laquelle nous devront réflechir. 

## Semaine 6 : 16/03 - 22/03

Nous sommes toujours bloquées sur ces 2 derniers aspects que nous nous sommes répartis :
récupération du contenu d'un fichier et mise à jour de l'affichage lors d'un changement dans la zone de texte.

Du côté du bouton permettant l'ajout d'un fichier `.json`, nous avons réussi à récupérer le contenu du fichier dans le code HTML
et à l'afficher sur Grafana (juste le texte du fichier), mais nous n'arrivons pas à passer le fichier depuis le fichier `.html`
à la classe `TrackMapCtrl` qui permet de faire l'affichage des objets avec Leaflet.
Comme le problème vient du fait que nous ne connaissons pas assez Javascript et AngularJS nous sous sommes renseignées sur ces technologies cette semaine.
En espérant pouvoir nous débloquer rapidement. 

Pour l'instant, l'implémentation intuitive qui permet de retirer un objet GeoJSON inséré via la zone de texte ne fonctionne pas après plusieurs essais.

## Semaine 7 : 23/03 - 29/03

Nous avons réussi à implémenter l'importation d'un fichier et l'affichage de son contenu sur la carte. 

Nous n'avons pas pu utiliser le data binding d'AngularJS via `ng-model` car il n'est pas supporté pour *input[file]* 
([voir ici](https://docs.angularjs.org/api/ng/directive/input))

Nous avons récupéré le fichier grâce à la méthode `getElementById` et nous avons lu son contenu grâce à un `FileReader()`.

Nous avons réglé le problème de carte qui ne s'actualisait pas en même temps que le contenu de la `textarea`.
Le souci était que nous ne stockions pas la bonne donnée en mémoire pour la supprimer lorsque le texte était modifié,
c'était donc un nouvel objet (créé à partir de cette donnée et qui n'avait par conséquent jamais été ajouté à la carte) qui était "supprimé".
Maintenant lorsque le code de l'overlay est modifié, la carte est actualisée en temps réel.

## Semaine 8 : 30/03 - 05/04

Cette semaine nous avons réussi à faire une première version de liste d'overlays :
en-dessous de la `textarea` se trouve un bouton "Save" et lorsqu'on clique dessus le contenu de la `textarea` est sauvegardé comme un nouvel élément de la liste.
De plus, à côté du nom (pour l'instant c'est l'identifiant Leaflet) se trouvent 3 boutons (pour le moment inactifs) 
qui permettront de supprimer un élément, l'éditer (dans la `textarea`) ou le télécharger (en tant que fichier).

*TODO :*

- Implémenter les fonctions des 3 boutons
- Ajouter comme deuxième remote le Gitlab afin de pouvoir push sur les deux
([Github](https://github.com/micamur/grafana-trackmap-panel) et [Gitlab](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/19-20/12/grafana-trackmap-panel))
- Éditer le fichier README.md du projet afin de documenter nos ajouts
- Faire bientôt une pull request avec un MVP (Minimum Viable Product)

## Semaine 9 : 06/04 - 12/04

Cette semaine nous avons commencé l'implémentation des fonctions correspondantes aux 3 boutons des la liste des overlays.
On peut maintenant télécharger et supprimer un overlay.

Nous avons également amélioré l'aspect graphique du bouton qui permet d'ajouter un overlay GeoJson depuis un fichier pour qu'il corresponde au style graphique du plugin.
Nous avons également amélioré l'ajout du fichier, avant il fallait cliquer sur le bouton `browse` pour récupérer un fichier puis sur le bouton `import` pour l'ajouter à la carte.
Maintenant il n'y a qu'un seul bouton pour les deux opérations, lorsque le fichier est séléctionné il est automatiquement ajouté à la carte.

## Semaine 10 : 13/04 - 19/04

Cette semaine nous avons commencé la fonction qui permet l'édition d'un overlay de la liste.
Et nous avons ajouté les popups qui permettent d'afficher les `properties` du fichier geojson lors d'un clique sur l'overlay.
Nous avons aussi commencé à chercher comment faire notre propre plugin grafana pour implémenter l'ajout du QRcode.
[How to start your own plugin](https://grafana.com/docs/grafana/v6.6/plugins/developing/development/)

## Semaine 11 : 27/04 - 03/05

Cette semaine nous nous sommes rendues compte d'une erreur lors de la sauvegarde du dashboard.
Nous sommes donc remontées dans les commits pour voir où apparaissait l'erreur et trouver d'où elle provenait. 
Mica s'est chargé de corriger cette erreur, cela provenait du fait que certaines données (notamment les objets Leaflet) ne sont pas bien sérialisées avec AngularJS et Javascript.
Pour contourner le problème nous avons utilisé des listes annexes.

Pendant ce temps, Claire s'est chargée de créer un nouveau panel en suivant les étapes données [ici](https://grafana.com/tutorials/build-a-panel-plugin/#1) et a implémenté la génération d'un QR code pour le dashboard courant.

Nous avons décidé de ne pas aller au bout de la fonction d'édition pour le plugin Trackmap car nous n'avons pas pu la finir dans les temps.