# Setup

## Grafana

Download Gafana [here](https://grafana.com/grafana/download) and follow the installation instructions for Debian or Ubuntu [here](https://grafana.com/docs/grafana/latest/installation/debian/#2-start-the-server).

If you have another operating system you will find more information [here](https://grafana.com/docs/grafana/latest/installation/).

To get familiar with Grafana follow the [Getting Started](https://grafana.com/docs/grafana/latest/guides/getting_started/) guide.

## TrackMap

To get the latest version of the plugin or do development follow the [README](https://github.com/pR0Ps/grafana-trackmap-panel/blob/master/README.md) instructions of the Github project.

## QRCode 

To install and build the plugin on a local Grafana you will need `yarn` (`npm install -g yarn`).

First clone the project in the folder `/var/lib/grafana/plugins`.

Then you need to install the dependencies by running:

``` 
yarn install 
```

After that you can build the plugin by running the following command:

``` 
yarn build 
```

You can also run the code in development mode with:

``` 
yarn dev 
```

Finally, don't forget to restart the Grafana server with the folowing command:

``` 
sudo service grafana-server restart 
```

And go to `http://localhost:3000/`.
