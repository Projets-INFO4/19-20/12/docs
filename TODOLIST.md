# Idées d'améliorations possibles pour le plugin Trackmap

- Possibilité de mettre plusieurs overlays
    - Affichage en liste d'un overlay : pour permettre la gestion de plusieurs overlays
    - Ajouter/changer le nom d'un overlay
    - Supprimer un overlay
        - Annuler la dernière suppression
    - Télécharger un overlay (à partir du site) : cela permet de récupérer le code qui a été précédemment uploadé pour l'éditer avec d'autres outils que juste la boîte de code sur Grafana
    - Éditer et sauvegarder un overlay dans la boîte de code : pour faire une édition ou une visualisation rapide
    - Changer l'ordre des overlays dans la liste, ce qui les déplace sur la carte
- Récupérer les infos dans "properties" des "features" des GeoJSON et affichage d'une popup en cliquant/passant la souris sur une figure