# Improving Grafana plugins

Our project goal is to improve an already existing [Grafana](https://grafana.com/) plugin and to create a new one.

## TrackMap

[TrackMap](https://github.com/pR0Ps/grafana-trackmap-panel/blob/master/README.md) is an already existing panel for Grafana that visualizes GPS points as a line on an interactive map.
We added the possibility to add GeoJSON data to the map by copy/pasting the data in a text area or by importing a file via a button.
We also provide other features like a list of overlays and the possibility to download or delete each overlay and a popup feature to display properties information on an overlay click.

Our commits are in the [`GeoJSON-overlay`](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/19-20/12/grafana-trackmap-panel/-/tree/GeoJSON-overlay) branch.
We have written a [pull request](https://github.com/pR0Ps/grafana-trackmap-panel/pull/27) to the author of the plugin.

## QRCode

We provide a panel that can be added to a Dashboard and that displays a QR code of the current Dashboard.
If you scan the QR code of a local dashboard (the URL starts with `localhost:port`) with a smartphone, it wont detect the URL as the URL will only work on the computer that's running the website.
However, the URL represented by the QR code can be checked with an [online QR code decoder](https://zxing.org/w/decode.jspx).
We tried to do a pull request to contribute the plugin to the community but it didn't pass the test.
We tried several things but we didn't understand where the error came from and how to correct it in time.
The link the the failing pull request is [here](https://github.com/grafana/grafana-plugin-repository/pull/614).

## Demo

Here is a [quick demo video](https://youtu.be/GAh3mlZPDpE) of the two panels, how to use them and what the results look like.

## Logbook

You can find our project logbook [here](Amélioration_plugin_Grafana_info4_2019_2020.md).

## Setup

You can find the setup instructions for Grafana and the differents panels [here](SETUP.md).

## Final Report

You can find our final report [here](final-report.pdf).